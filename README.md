# NardnetCommons-authorization-server

## Introduction

The Nardnet Commons Authorization Server manages authorization tokens using Redis for scalability. It includes necessary database migrations found in `/NardnetCommons-authorization-server/src/main/resources/db/migration`. If you run the Uphill Backend application, the required tables are already created there. To disable Flyway migrations, set `spring.flyway.enabled=false`.


## Clone Repository

Clone with HTTPS GitLab:

```bash
git clone https://gitlab.com/nardnet/nardnetcommons-authorization-server.git
```

# NardnetCommons-resource-server

## Introduction

The Nardnet Commons Resource Server serves as a dependency for handling resource requests and auditing functionalities. It extends AbstractAuditingEntity for easy entity auditing.

## Clone Repository

Clone with HTTPS GitLab:

```bash
git clone https://gitlab.com/nardnet/nardnetcommons-resource-server.git
```

# Uphill-backend

## Introduction

Backend 

## Clone Repository

Clone with HTTPS GitLab:

```bash
git clone https://gitlab.com/nardnet/uphill-backend.git
```

# Package Registry nardnetArtifactory

Find the package on GitLab Package Registry:

[Package Registry](https://gitlab.com/nardnet/nardnetArtifactory/-/packages/23316002)

```
<dependency>
  <groupId>com.nardnet</groupId>
  <artifactId>NardnetCommons-resource-server</artifactId>
  <version>1.0.4-SNAPSHOT</version>
</dependency>

```

## Docker Commands


### MySql 

```bash
docker run -it -d --name mysql-container -p 3306:3306 --network uphillNet -e  MYSQL_ALLOW_EMPTY_PASSWORD=yes --restart always -v mysql_data_container:/var/lib/mysql mysql:8

```

### Redis 

```bash

docker run -it -d --name redis-container -p 6379:6379 --network uphillNet --restart always -v redisdb_data_container:/data/db redis:alpine3.19  

```

### uphillhealth-api 

```bash

docker container run -it -d --name uphillhealth-api --rm -p 8080:8080 -e DB_HOST=mysql-container -e DB_SERVER_HOST=localhost -e DB_SERVER_JWKS_HOST=uphillhealth-authorization-server --network uphillNet 42032178/uphillhealth-api 

```

### Uphillhealth-authorization-server 

```bash

docker container run -it -d --name uphillhealth-authorization-server --rm -p 8081:8081 -e DB_HOST=mysql-container -e DB_REDIS_HOST=redis-container --network uphillNet 42032178/nardnenommons-authorization-server

```

## Installation files

The afterMigration file (`src/main/resources/db/testdata/afterMigrate.sql`) includes necessary tables and configurations.

### Table aut_user

Contains the user used to obtain the password token.

![oath Token](oath_token.png)

### Table oauth_client_details

Contains Client IDs used to obtain various grant types.

![oath Clients](oath_clients.png)

![oath Token Auth](oath_token_auth.png)

### Manual Installation 

Clone repositories:


* [ Nardnet Commons Authorization Server ](https://gitlab.com/nardnet/nardnetcommons-resource-server.git)

* [ Uphill Backend ](https://gitlab.com/nardnet/uphill-backend.git)

* [Nardnet Commons Resource Server](https://gitlab.com/nardnet/nardnetcommons-resource-server.git) (if Maven dependency isn't working)


### Automatic Installation

Docker Compose or Projects (GitHub)

```

docker compose up --scale uphillhealth-api=2 (Not Recommended)

docker run -it -d --name mysql-container -p 3306:3306 --network uphillNet -e  MYSQL_ALLOW_EMPTY_PASSWORD=yes --restart always -v mysql_data_container:/var/lib/mysql mysql:8

docker run -it -d --name redis-container -p 6379:6379 --network uphillNet --restart always -v redisdb_data_container:/data/db redis:alpine3.19  

docker container run -it -d --name uphillhealth-api --rm -p 8080:8080 -e DB_HOST=mysql-container -e DB_SERVER_HOST=localhost -e DB_SERVER_JWKS_HOST=uphillhealth-authorization-server --network uphillNet 42032178/uphillhealth-api

docker container run -it -d --name uphillhealth-authorization-server --rm -p 8081:8081 -e DB_HOST=mysql-container -e DB_REDIS_HOST=redis-container --network uphillNet 42032178/nardnenommons-authorization-server
```

# Good to know:

Access Swagger UI at:

[Swagger UI](http://localhost:8080/swagger-ui/index.html)


## Authorize

![Security auth](security_auth.png) 

![Available auth](available.png) 

## Post Receive Message

![Receive Message](receiveMessage.png) 


### Request body

```
{
    "resourceType": "Bundle",
    "type": "message",
    "entry": [
        {
            "resource": {
                "resourceType": "MessageHeader",
                "focus": {
                    "reference": "Encounter/123"
                }
            }
        },
        {
            "resource": {
                "resourceType": "Encounter",
                "id": "123",
                "identifier": [
                    {
                        "system": "urn:uh-encounter-id",
                        "value": "ABC123"
                    }
                ],
                "status": "finished",
                "type": [
                    {
                        "coding": [
                            {
                                "system": "http://hl7.org/fhir/ValueSet/service-type",
                                "code": "outpatient"
                            }
                        ]
                    }
                ],
                "subject": {
                    "reference": "Patient/456"
                }
            }
        },
        {
            "resource": {
                "resourceType": "Patient",
                "id": "456",
                "identifier": [
                    {
                        "system": "urn:uh-patient-id",
                        "value": "DEF456"
                    }
                ],
                "name": [
                    {
                        "family": "Doe RaRaRa",
                        "given": [
                            "John"
                        ]
                    }
                ],
                "gender": "male",
                "birthDate": "1980-01-01"
            }
        }
    ]
}

```

## Get Patient Id

![Patient Id](PatientId.png) 




#  Problem Uncovered: Unveiling Application Errors:

* 1 ) When trying to execute the integration test EncounterControllerIT, Flyway is unable to find the database URL.


![Error Test](ErrorApplicationPropertiesTest.png) 


* 2 )  When I run the docker-compose, I get an error of property not found.

![Propertie Dynamic](ErrorDynamicPropertie.png) 
