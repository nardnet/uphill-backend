package com.uphill.infrastructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.nardnet.commons.security.infrastructure.AuthSecutiry;
import com.nardnet.commons.security.infrastructure.audit.AuditingService;
import com.nardnet.commons.security.infrastructure.config.ResourceServerConfig;

@Configuration
public class ExternalConfig {

	@Bean
	AuditingService auditingService() {
		return new AuditingService();

	}

	@Bean
	AuthSecutiry authSecutiry() {
		return new AuthSecutiry();
	}

	@Bean
	ResourceServerConfig resourceServer() {
		return new ResourceServerConfig();
	}	
}
