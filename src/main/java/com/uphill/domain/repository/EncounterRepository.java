package com.uphill.domain.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.uphill.domain.model.Encounter;

@Repository
public interface EncounterRepository extends CustomJpaRepository<Encounter, Long> {
	
	Optional<Encounter> findByEncounterId(String encounterId);
	
	Optional<Encounter> findByIdentifierValue(String encounterId);
	
	Optional<Encounter> findByCode(String code);

}

