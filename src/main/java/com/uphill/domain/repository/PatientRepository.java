package com.uphill.domain.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.uphill.domain.model.Patient;

@Repository
public interface PatientRepository extends CustomJpaRepository<Patient, Long> {
	
	Optional<Patient> findByPatientId(String patientId);
	
	Optional<Patient> findByIdentifierValue(String patientId);
	
	Optional<Patient> findByCode(String code);

}
