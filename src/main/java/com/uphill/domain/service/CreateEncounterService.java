package com.uphill.domain.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uphill.domain.exception.EncounterNotFoundException;
import com.uphill.domain.exception.EntityInUseException;
import com.uphill.domain.model.Encounter;
import com.uphill.domain.repository.EncounterRepository;

@Service
public class CreateEncounterService {
	
	private static final String MSG_ENCOUNTER_IN_USE 
	= "Encouter code %d cannot be removed as it is in use.";

	@Autowired
	private EncounterRepository encounterReposistory;

	@Transactional
	public Encounter save(Encounter encounter) {
		Encounter encounterOld;		
		
		String encounterId = encounter.getEncounterId();

		Optional<Encounter> optionalEncounter = encounterReposistory.findByEncounterId(encounterId);

		if (optionalEncounter.isPresent()) {
			encounterOld = optionalEncounter.get();
			BeanUtils.copyProperties(encounter,encounterOld, "id", "code", "encounterId", "patient");
			BeanUtils.copyProperties(encounter.getPatient(),encounterOld.getPatient(), "id","code","patientId");		
			
		} else {
			encounterOld = encounter;
		}

		return encounterReposistory.save(encounterOld);
	}
	
	@Transactional
	public void remove(Long encounterId) {
		try {
			encounterReposistory.deleteById(encounterId);
			encounterReposistory.flush();
			
		} catch (EmptyResultDataAccessException e) {
			throw new EncounterNotFoundException(encounterId);
		
		} catch (DataIntegrityViolationException e) {
			throw new EntityInUseException(
				String.format(MSG_ENCOUNTER_IN_USE, encounterId));
		}
	}
	
	public Encounter findOrFail(String encounterId) {
		return encounterReposistory.findByIdentifierValue(encounterId)
			.orElseThrow(() -> new EncounterNotFoundException(encounterId));
	}

}
