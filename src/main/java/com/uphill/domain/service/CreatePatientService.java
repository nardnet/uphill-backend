package com.uphill.domain.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uphill.domain.exception.EntityInUseException;
import com.uphill.domain.exception.PatientNotFoundException;
import com.uphill.domain.model.Patient;
import com.uphill.domain.repository.PatientRepository;

@Service
public class CreatePatientService {

	private static final String MSG_PATIENT_IN_USE = "Patient code %d cannot be removed as it is in use.";

	@Autowired
	private PatientRepository patientRepository;

	@Transactional
	public Patient save(Patient patient) {
		Patient patientOld;

		String patientId = patient.getPatientId();

		Optional<Patient> optionalPatient = patientRepository.findByPatientId(patientId);

		if (optionalPatient.isPresent()) {
			patientOld = optionalPatient.get();
			BeanUtils.copyProperties(patient, patientOld, "id", "code");
		} else {
			patientOld = patient;
		}

		return patientRepository.save(patientOld);
	}

	@Transactional
	public void remove(Long patientId) {
		try {
			patientRepository.deleteById(patientId);
			patientRepository.flush();

		} catch (EmptyResultDataAccessException e) {
			throw new PatientNotFoundException(patientId);

		} catch (DataIntegrityViolationException e) {
			throw new EntityInUseException(String.format(MSG_PATIENT_IN_USE, patientId));
		}
	}

	public Patient findOrFail(String patientId) {
		return patientRepository.findByIdentifierValue(patientId).orElseThrow(() -> new PatientNotFoundException(patientId));
	}

}
