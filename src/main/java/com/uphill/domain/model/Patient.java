package com.uphill.domain.model;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.nardnet.commons.security.infrastructure.audit.AbstractAuditingEntity;

@Entity
@Table(name = "uph_patient")
public class Patient extends AbstractAuditingEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "code", nullable = false)
	private String code;

	@Column(name = "patient_id", nullable = false)
	private String patientId;

	@Column(name = "identifier_system", nullable = false)
	private String identifierSystem;

	@Column(name = "identifier_value", nullable = false)
	private String identifierValue;

	@Column(name = "given_name", nullable = false)
	private String givenName;

	@Column(name = "family_name")
	private String familyName;

	@Column(name = "gender")
	private String gender;

	@Column(name = "birth_date")
	private OffsetDateTime birthDate;

	@OneToMany(mappedBy = "patient")
	private List<Encounter> encounters = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPatientId() {
		return patientId;
	}	

	public String getIdentifierSystem() {
		return identifierSystem;
	}

	public void setIdentifierSystem(String identifierSystem) {
		this.identifierSystem = identifierSystem;
	}

	public String getIdentifierValue() {
		return identifierValue;
	}

	public void setIdentifierValue(String identifierValue) {
		this.identifierValue = identifierValue;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public OffsetDateTime getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(OffsetDateTime birthDate) {
		this.birthDate = birthDate;
	}

	public List<Encounter> getEncounters() {
		return encounters;
	}

	public void setEncounters(List<Encounter> encounters) {
		this.encounters = encounters;
	}

}
