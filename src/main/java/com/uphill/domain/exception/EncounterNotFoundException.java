package com.uphill.domain.exception;

import java.util.UUID;

public class EncounterNotFoundException extends EntityNotFoundException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EncounterNotFoundException(String message) {
		super(message);		
	}
	
	public EncounterNotFoundException(UUID encounterId) {
		this(String.format("There is no encounter with code %d", encounterId));
	}
	
	public EncounterNotFoundException(Long encounterId) {
		this(String.format("There is no encounter with code %d", encounterId));
	}


}
