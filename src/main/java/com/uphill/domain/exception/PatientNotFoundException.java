package com.uphill.domain.exception;

import java.util.UUID;

public class PatientNotFoundException extends EntityNotFoundException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PatientNotFoundException(String message) {
		super(message);		
	}
	
	public PatientNotFoundException(UUID patientId) {
		this(String.format("There is no patient with code %d", patientId));
	}
	
	public PatientNotFoundException(Long patientId) {
		this(String.format("There is no patient with code %d", patientId));
	}

}
