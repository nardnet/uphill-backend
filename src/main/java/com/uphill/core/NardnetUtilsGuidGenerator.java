package com.uphill.core;

import java.util.UUID;

public class NardnetUtilsGuidGenerator {

	public static String generateRandom() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	public static String generateRandomStandard() {
		return UUID.randomUUID().toString();
	}

}
