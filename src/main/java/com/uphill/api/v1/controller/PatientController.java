package com.uphill.api.v1.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uphill.api.v1.assembler.PatientModelAssembler;
import com.uphill.api.v1.model.PatientModel;
import com.uphill.api.v1.openapi.controller.PatientControllerOpenApi;
import com.uphill.domain.service.CreatePatientService;



@RestController
@RequestMapping(value="/v1/patients", produces = MediaType.APPLICATION_JSON_VALUE)
public class PatientController implements PatientControllerOpenApi{
	
	private static final Logger logger = LoggerFactory.getLogger(PatientController.class);
		
	@Autowired
	PatientModelAssembler patientModelAssembler;
	
	@Autowired
	CreatePatientService patientService;	
	
	
    @GetMapping("/{id}")
    public PatientModel getPatientById(@PathVariable String id) {
    	return patientModelAssembler.toModel(patientService.findOrFail(id));
    }   

}
