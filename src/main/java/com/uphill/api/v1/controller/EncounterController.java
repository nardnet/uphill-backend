package com.uphill.api.v1.controller;

import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Encounter;
import org.hl7.fhir.r4.model.Encounter.EncounterStatus;
import org.hl7.fhir.r4.model.ResourceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.uphill.api.v1.assembler.EncounterFhirInputModelDisassembler;
import com.uphill.api.v1.assembler.EncounterInputModelDisassembler;
import com.uphill.api.v1.assembler.EncounterModelAssembler;
import com.uphill.api.v1.model.EncounterModel;
import com.uphill.api.v1.openapi.controller.EncounterControllerOpenApi;
import com.uphill.domain.exception.BusinessException;
import com.uphill.domain.service.CreateEncounterService;

import ca.uhn.fhir.context.FhirContext;

@RestController
@RequestMapping(value = "/v1/encounters", produces = MediaType.APPLICATION_JSON_VALUE)
public class EncounterController implements EncounterControllerOpenApi {

	@Autowired
	private EncounterFhirInputModelDisassembler encounterFhirInputModelDisassembler;

	@Autowired
	private EncounterInputModelDisassembler encounterInputModelDisassembler;

	@Autowired
	private EncounterModelAssembler encounterModelAssembler;

	@Autowired
	private CreateEncounterService createEncounterService;

	private static final Logger logger = LoggerFactory.getLogger(EncounterController.class);

	@PostMapping("/receiveMessage")
	@ResponseStatus(HttpStatus.CREATED)
	public EncounterModel receiveMessage(@RequestBody String encounterJson) {
		EncounterModel encounterIn = null;

		try {
			Bundle bundle = FhirContext.forR4().newJsonParser().parseResource(Bundle.class, encounterJson);
			for (Bundle.BundleEntryComponent entry : bundle.getEntry()) {

				if (entry.hasResource() && entry.getResource().getResourceType().equals(ResourceType.Encounter)) {
					Encounter encounter = (Encounter) entry.getResource();

					if (validateEncounterWithPatient(encounter)) {

						encounterIn = encounterFhirInputModelDisassembler.toDomainObject(encounter);
						return encounterModelAssembler.toModel(createEncounterService
								.save(encounterInputModelDisassembler.toDomainObject(encounterIn)));
					}
				}
			}

		} catch (Exception e) {
			throw new BusinessException(e.getMessage(), e);
		}

		return encounterIn;

	}

	@GetMapping("/{id}")
	public EncounterModel getEncounterById(@PathVariable String encounterId) {
		return encounterModelAssembler.toModel(createEncounterService.findOrFail(encounterId));

	}

	private boolean validateEncounterWithPatient(Encounter encounter) {

		// Checks if there is at least one identifier
		if (encounter.getId().isEmpty()) {
			return false;
		}

		// Checks if the encounter status is valid
		EncounterStatus status = encounter.getStatus();
		if (status == null || !status.toCode()
				.matches("planned|arrived|in-progress|onleave|finished|cancelled|entered-in-error")) {
			return false;
		}

		// Checks if the service type is valid
		boolean validServiceType = false;
		for (CodeableConcept type : encounter.getType()) {
			for (Coding coding : type.getCoding()) {
				if ("http://hl7.org/fhir/ValueSet/service-type".equals(coding.getSystem())) {
					validServiceType = true;
					break;
				}
			}
			if (validServiceType) {
				break;
			}
		}
		if (!validServiceType) {
			return false;
		}

		// Verifies that the patient reference is present and valid
		if (encounter.getSubject() == null || encounter.getSubject().getReferenceElement().getResourceType() == null
				|| !encounter.getSubject().getReferenceElement().getResourceType().equals("Patient")) {
			return false;
		}

		return true;
	}
}
