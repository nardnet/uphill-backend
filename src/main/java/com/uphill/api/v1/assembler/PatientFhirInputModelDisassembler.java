package com.uphill.api.v1.assembler;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

import org.hl7.fhir.r4.model.Patient;
import org.springframework.stereotype.Component;

import com.uphill.api.v1.model.PatientModel;
import com.uphill.core.NardnetUtilsGuidGenerator;

@Component
public class PatientFhirInputModelDisassembler {


	public PatientModel toDomainObject(Patient patientInput) {
		
		PatientModel model = new PatientModel();

		if (Objects.nonNull(patientInput.getBirthDate())) {
			OffsetDateTime offsetDateTime = patientInput.getBirthDate().toInstant().atOffset(ZoneOffset.UTC);
			model.setBirthDate(offsetDateTime);
		}
		model.setCode(NardnetUtilsGuidGenerator.generateRandom());
		patientInput.getIdentifier().stream()
	    .forEach(identifier -> {
	    	model.setIdentifierSystem(identifier.getSystem());
	    	model.setIdentifierValue(identifier.getValue());	      
	    });
		
		model.setPatientId(patientInput.getId());
		model.setGivenName(patientInput.getNameFirstRep().getGivenAsSingleString());
		model.setFamilyName(patientInput.getNameFirstRep().getFamily());
		model.setGender(patientInput.getGender().getDisplay());
		
		return model;
	}

}
