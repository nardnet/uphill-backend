package com.uphill.api.v1.assembler;

import java.util.List;
import java.util.Objects;

import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Encounter;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uphill.api.v1.model.EncounterModel;
import com.uphill.api.v1.model.PatientModel;
import com.uphill.core.NardnetUtilsGuidGenerator;

@Component
public class EncounterFhirInputModelDisassembler {
	
	@Autowired
	PatientFhirInputModelDisassembler patientInputModelDisassembler;

	
	public  EncounterModel toDomainObject(Encounter encounterInput) {
		EncounterModel model = new EncounterModel();
		
		if (Objects.isNull(encounterInput)) {
			return null;
		}
		model.setCode(NardnetUtilsGuidGenerator.generateRandom());
		model.setEncounterId(encounterInput.getId());	
		model.setServiceType(getServiceType(encounterInput));
		model.setStatus(encounterInput.getStatus().toCode());
		
		encounterInput.getIdentifier().stream()
	    .forEach(identifier -> {
	    	model.setIdentifierSystem(identifier.getSystem());
	    	model.setIdentifierValue(identifier.getValue());	      
	    });
		
		Reference patientReference = encounterInput.getSubject();

		Patient patientResource = (Patient) patientReference.getResource();
		PatientModel patientModel = patientInputModelDisassembler.toDomainObject(patientResource);
		patientModel.getEncounters().add(model);
		model.setPatient(patientModel);		
		
		return model;
	}
	
	
	
	public String getServiceType(Encounter encounterInput) {
		String serviceTypeCode = null;
		List<CodeableConcept> serviceTypes = encounterInput.getType();

		if (!serviceTypes.isEmpty()) {

			for (CodeableConcept serviceType : serviceTypes) {
				// (http://hl7.org/fhir/ValueSet/service-type)
				if ("http://hl7.org/fhir/ValueSet/service-type".equals(serviceType.getCodingFirstRep().getSystem())) {

					serviceTypeCode = serviceType.getCodingFirstRep().getCode();

				}
			}
		}

		return serviceTypeCode;

	}

}
