package com.uphill.api.v1.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.uphill.api.v1.UpHillLinks;
import com.uphill.api.v1.controller.PatientController;
import com.uphill.api.v1.model.PatientModel;
import com.uphill.domain.model.Patient;

@Component
public class PatientModelAssembler extends RepresentationModelAssemblerSupport<Patient, PatientModel> {
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private UpHillLinks upHillLinks;
	
	public PatientModelAssembler() {
		super(PatientController.class, PatientModel.class);
	}
	
	@Override
	public PatientModel toModel(Patient patient) {
		PatientModel patientModel = createModelWithId(patient.getIdentifierValue(), patient);
		modelMapper.map(patient, patientModel);
		
		patientModel.add(upHillLinks.linkToPatients("patients"));
		
		return patientModel;
	}

}
