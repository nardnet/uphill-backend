package com.uphill.api.v1.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.uphill.api.v1.UpHillLinks;
import com.uphill.api.v1.controller.EncounterController;
import com.uphill.api.v1.model.EncounterModel;
import com.uphill.domain.model.Encounter;

@Component
public class EncounterModelAssembler extends RepresentationModelAssemblerSupport<Encounter, EncounterModel> {
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private UpHillLinks upHillLinks;
	
	public EncounterModelAssembler() {
		super(EncounterController.class, EncounterModel.class);
	}
	
	@Override
	public EncounterModel toModel(Encounter encouter) {
		EncounterModel encouterModel = createModelWithId(encouter.getIdentifierValue(), encouter);
		modelMapper.map(encouter, encouterModel);
		
		encouterModel.add(upHillLinks.linkToEncounters("encouters"));
		
		return encouterModel;
	}

}
