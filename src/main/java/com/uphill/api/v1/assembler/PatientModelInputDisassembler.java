package com.uphill.api.v1.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uphill.api.v1.model.PatientModel;
import com.uphill.core.NardnetUtilsGuidGenerator;
import com.uphill.domain.model.Patient;

@Component
public class PatientModelInputDisassembler {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public Patient toDomainObject(PatientModel patientInput) {
		if(patientInput.getCode() == null) {
			patientInput.setCode(NardnetUtilsGuidGenerator.generateRandom());
		}
		return modelMapper.map(patientInput, Patient.class);
	}

}
