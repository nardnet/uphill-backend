package com.uphill.api.v1.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uphill.api.v1.model.EncounterModel;
import com.uphill.core.NardnetUtilsGuidGenerator;
import com.uphill.domain.model.Encounter;

@Component
public class EncounterInputModelDisassembler {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public Encounter toDomainObject(EncounterModel encouterInput) {
		if(encouterInput.getCode() == null) {
			encouterInput.setCode(NardnetUtilsGuidGenerator.generateRandom());
		}
		
		return modelMapper.map(encouterInput, Encounter.class);
	}

}
