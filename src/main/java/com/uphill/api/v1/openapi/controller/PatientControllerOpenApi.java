package com.uphill.api.v1.openapi.controller;

import com.uphill.api.v1.model.PatientModel;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

@SecurityRequirement(name = "security_auth")
@Tag(name = "Patient")
public interface PatientControllerOpenApi {
	
	@Operation(summary = "Find Patient by Id", description = "Patient resource must be obtained by its identifier with system urn:uh-patient-id ")
	PatientModel getPatientById(@Parameter(description = "[identifierValue] - Identifier with value urn:uh-patient-id ", example = "DEF456", required = true) String patientId);

}
