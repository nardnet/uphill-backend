package com.uphill.api.v1.openapi.controller;

import com.uphill.api.v1.model.EncounterModel;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

@SecurityRequirement(name = "security_auth")
@Tag(name = "Encounters")
public interface EncounterControllerOpenApi {
	
	
	@Operation(summary = "Encounter Registration", description = "MessageHeader's focus is an Encounter resource;")
	EncounterModel receiveMessage(String encounterJson);
	
	@Operation(summary = "Find Encouter by Id", description = "Encounter resource must be obtained by its identifier with system urn:uh-encounter-id ")
	EncounterModel getEncounterById(@Parameter(description = "[identifierValue] - Identifier with value urn:uh-encounter-id ", example = "ABC123", required = true) String encounterId);

}
