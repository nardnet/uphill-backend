package com.uphill.api.v1;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

import com.uphill.api.v1.controller.EncounterController;
import com.uphill.api.v1.controller.PatientController;

@Component
public class UpHillLinks {
	
	public Link linkToPatients() {
		return linkToPatients(IanaLinkRelations.SELF.value());
	}
	
	public Link linkToPatients(String rel) {
		return linkTo(PatientController.class).withRel(rel);
	}
	
	public Link linkToEncounters() {
		return linkToEncounters(IanaLinkRelations.SELF.value());
	}
	
	public Link linkToEncounters(String rel) {
		return linkTo(EncounterController.class).withRel(rel);
	}

}
