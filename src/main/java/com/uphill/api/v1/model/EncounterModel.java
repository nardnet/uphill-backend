package com.uphill.api.v1.model;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;

@Relation(collectionRelation = "encounters")
public class EncounterModel extends RepresentationModel<EncounterModel> {

	@JsonIgnore
	private Long id;

	@Schema(example = "0b93eef775584b4f855f795f60e6d9b8")
	private String code;

	@Schema(example = "Encounter/123")
	private String encounterId;

	@Schema(example = "urn:uh-encounter-id")
	private String identifierSystem;

	@Schema(example = "ABC123")
	private String identifierValue;

	@Schema(example = "finished")
	private String status;

	@Schema(example = "outpatient")
	private String serviceType;

	private PatientModel patient;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEncounterId() {
		return encounterId;
	}

	public void setEncounterId(String encounterId) {
		this.encounterId = encounterId;
	}

	public String getIdentifierSystem() {
		return identifierSystem;
	}

	public void setIdentifierSystem(String identifierSystem) {
		this.identifierSystem = identifierSystem;
	}

	public String getIdentifierValue() {
		return identifierValue;
	}

	public void setIdentifierValue(String identifierValue) {
		this.identifierValue = identifierValue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public PatientModel getPatient() {
		return patient;
	}

	public void setPatient(PatientModel patient) {
		this.patient = patient;
	}

}
