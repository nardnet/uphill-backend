package com.uphill.api.v1.model;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;

@Relation(collectionRelation = "patients")
public class PatientModel extends RepresentationModel<PatientModel> {

	@JsonIgnore
	private Long id;

	@Schema(example = "ba0737e8f09445f4b2342d687a8bf900")
	private String code;

	@Schema(example = "Patient/456")
	private String patientId;

	@Schema(example = "urn:uh-patient-id")
	private String identifierSystem;

	@Schema(example = "DEF456")
	private String identifierValue;

	@Schema(example = "John")
	private String givenName;

	@Schema(example = "Doe")
	private String familyName;

	@Schema(example = "Male")
	private String gender;

	@Schema(example = "1980-01-01")
	private OffsetDateTime birthDate;

	@JsonIgnore
	private List<EncounterModel> encounters = new ArrayList<>();

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getIdentifierSystem() {
		return identifierSystem;
	}

	public void setIdentifierSystem(String identifierSystem) {
		this.identifierSystem = identifierSystem;
	}

	public String getIdentifierValue() {
		return identifierValue;
	}

	public void setIdentifierValue(String identifierValue) {
		this.identifierValue = identifierValue;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public OffsetDateTime getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(OffsetDateTime birthDate) {
		this.birthDate = birthDate;
	}

	public List<EncounterModel> getEncounters() {

		if (Objects.isNull(encounters)) {
			encounters = new ArrayList<>();
		}

		return encounters;
	}

	public void setEncounters(List<EncounterModel> encounters) {
		this.encounters = encounters;
	}

}
