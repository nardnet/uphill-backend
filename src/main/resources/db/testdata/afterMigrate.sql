set foreign_key_checks = 0;

lock tables aut_group write, aut_group_permission write, aut_permission write, aut_user write,
	aut_user_group write, uph_encounter write, uph_patient write, oauth_code write, oauth_client_details write;

delete from aut_user;
delete from oauth_client_details;

set foreign_key_checks = 1;

alter table aut_user auto_increment = 1;

insert into aut_user (id, name, email, password) values
(1, 'Rafael Gomes', 'nardnet@uphill.com', '$2a$12$AnuSxNbCLS8rxhJj9gnKDevtapB3.DRuAfXf2xLRIyE61GPvRJPRa');

insert into oauth_client_details (
  client_id, resource_ids, client_secret, 
  scope, authorized_grant_types, web_server_redirect_uri, authorities,
  access_token_validity, refresh_token_validity, autoapprove
)
values (
  'uphill-web', null, '$2a$12$AnuSxNbCLS8rxhJj9gnKDevtapB3.DRuAfXf2xLRIyE61GPvRJPRa',
  'READ,WRITE', 'password', null, null,
  60 * 60 * 6, 60 * 24 * 60 * 60, null
);

insert into oauth_client_details (
  client_id, resource_ids, client_secret, 
  scope, authorized_grant_types, web_server_redirect_uri, authorities,
  access_token_validity, refresh_token_validity, autoapprove
)
values (
  'uphill-analytics', null, '$2a$12$AnuSxNbCLS8rxhJj9gnKDevtapB3.DRuAfXf2xLRIyE61GPvRJPRa',
  'READ,WRITE', 'authorization_code', 'http://www.localhost:8080,http://localhost:8080/swagger-ui/oauth2-redirect.html,http://uphillhealth-api:8080/swagger-ui/oauth2-redirect.html,http://uphillhealth-api:8080', null,
  null, null, null
);

insert into oauth_client_details (
  client_id, resource_ids, client_secret, 
  scope, authorized_grant_types, web_server_redirect_uri, authorities,
  access_token_validity, refresh_token_validity, autoapprove
)
values (
  'uphill', null, '$2a$12$AnuSxNbCLS8rxhJj9gnKDevtapB3.DRuAfXf2xLRIyE61GPvRJPRa',
  'READ,WRITE', 'client_credentials', null, null,
  null, null, null
);

unlock tables;
