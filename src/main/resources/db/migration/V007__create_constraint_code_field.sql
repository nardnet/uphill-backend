alter table uph_patient add constraint uk_patient_code unique (code);
alter table uph_patient add constraint uk_patient_patient_id unique (patient_id);
alter table uph_encounter add constraint uk_encounter_code unique (code);
alter table uph_encounter add constraint uk_encounter_encounter_id unique (encounter_id);
alter table aut_user add constraint uk_user_code unique (code);
alter table aut_permission add constraint uk_permission_code unique (code);
alter table aut_group add constraint uk_group_code unique (code);
