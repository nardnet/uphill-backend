alter table uph_patient add identifier_system varchar(60) not null;
alter table uph_patient add identifier_value varchar(60) not null;
alter table uph_encounter add identifier_system varchar(60) not null;
alter table uph_encounter add identifier_value varchar(60) not null;