create table uph_patient (

    id bigint not null auto_increment,
 	code varchar(255) NOT NULL,
    patient_id varchar(255) NOT NULL,
    given_name varchar(255) NOT NULL,
    family_name varchar(255),
    gender varchar(50),
    birth_date datetime,
    created_by varchar(50),
    created_date timestamp,
    last_modified_by varchar(50),
    last_modified_date timestamp,

	primary key (id)
);

