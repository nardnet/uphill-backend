create table uph_encounter(

    id bigint not null auto_increment,
 	code varchar(255) NOT NULL,
    encounter_id varchar(255) NOT NULL,    
    status varchar(255) NOT NULL,       
    service_type varchar(50),
    uph_patient_id bigint NOT NULL,        
    created_by varchar(50),
    created_date timestamp,
    last_modified_by varchar(50),
    last_modified_date timestamp,

	primary key (id)
);


alter table uph_encounter add constraint fk_encounter_patient
foreign key (uph_patient_id) references uph_patient (id);