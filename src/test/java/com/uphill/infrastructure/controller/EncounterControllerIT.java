package com.uphill.infrastructure.controller;

import static io.restassured.RestAssured.given;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.hl7.fhir.r4.model.Encounter;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Reference;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;

import com.uphill.infrastructure.util.DatabaseCleaner;
import com.uphill.infrastructure.util.ResourceUtils;

import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/application-test.properties")
public class EncounterControllerIT {	
	
	@LocalServerPort
	private int port;
	
	String accessToken;
	
	@Autowired
	private DatabaseCleaner databaseCleaner;
	
	private Map<String, Encounter> encounters = new HashMap<>();
	private Map<String, Patient> patients = new HashMap<>();
	private String jsonMessage;
	
	@BeforeEach
	public void setUp() throws ParseException {		
		
		jsonMessage= ResourceUtils.getContentFromResource(
				"/json/message.json");
		
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		
		String tokenEndpoint = "http://localhost:8081/oauth/token";		
		String clientID = "uphill-web";
        String clientSecret = "web123";
    
        String username = "nardnet@uphill.com";
        String password = "web123";	
        
        Response response = RestAssured.given()
                .auth()
                .preemptive()
                .basic(clientID, clientSecret)
                .formParam("grant_type", "password")
                .formParam("username", username)
                .formParam("password", password)
                .post(tokenEndpoint);
        
        accessToken = response.jsonPath().getString("access_token");
     
		RestAssured.port = port;	
		databaseCleaner.clearTables();
		populateSampleData();
	}
	
	@Test
	public void shouldReturnStatus201Created() {	
		given()
		    .header("Authorization", "Bearer " + accessToken)
			.basePath("/v1/encounters/receiveMessage")			
			.body(jsonMessage)
			.accept(ContentType.JSON)
		.when()
		    .post()
		.then()
			.statusCode(HttpStatus.CREATED.value());
	}
	
	
    // Mock method to populate sample data
    private void populateSampleData() throws ParseException {
    	
    	Reference patientReference = new Reference();
    	patientReference.setReference("Patient/1"); 
    	
    	// Code to convert String to a Date object
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	Date birthDate = sdf.parse("1980-01-01");
    	
        Patient patient = new Patient();
        patient.addIdentifier().setSystem("urn:uh-patient-id").setValue("1");
        patient.addName().setFamily("Doe").addGiven("John");
        patient.setGender(Enumerations.AdministrativeGender.MALE);
        patient.setBirthDate(birthDate);
        patients.put("1", patient);
        
        patientReference.setResource(patient);
    	
        Encounter encounter = new Encounter();
        encounter.addIdentifier().setSystem("urn:uh-encounter-id").setValue("12345");
        encounter.setStatus(Encounter.EncounterStatus.FINISHED);
        encounter.addType().addCoding().setSystem("http://hl7.org/fhir/ValueSet/service-type").setCode("outpatient");
        encounter.setSubject(patientReference);
        encounters.put("12345", encounter);         
        
    }

	

}
